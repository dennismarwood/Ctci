'''Tests the doublyLinkedList module'''

from .teardown import TearDown
from ...doublyLinkedList import doublyLinkedList

class DoublyLinkedListTestCase(TearDown):
    '''Test the inserting and removal of nodes'''
    #pylint: disable=C0111
    #pylint: disable=W0104
    #pylint: disable=W0106
    #pylint: disable=W0201

    def setUp(self):
        super().setUp()
        self.correct_dll = doublyLinkedList(sentinel=self.a_node)
        self.correct_dll.length = 8
        self.test_dll = doublyLinkedList()

    def tearDown(self):
        super().tearDown()
        del self.correct_dll

    def test_init(self):
        dll = doublyLinkedList()
        with self.subTest():
            self.assertIsNone(dll.sentinel)
        with self.subTest():
            self.assertEqual(len(dll), 0)

        dll = doublyLinkedList(self.a_node)
        with self.subTest():
            self.assertIs(dll.sentinel, self.a_node)
        with self.subTest():
            self.assertEqual(0, len(dll))

    def test_eq(self):
        dll = doublyLinkedList(self.a_node)
        dll.length = 8
        with self.subTest():
            self.assertTrue(dll == self.correct_dll)

    def test_eq_raises_value_error_when_length_is_wrong(self):
        #length is wrong
        dll = doublyLinkedList(self.a_node)
        dll.length = 7
        msg = "{} vs {}".format(self.correct_dll.length, dll.length)
        with self.assertRaises(ValueError, msg=msg):
            dll == self.correct_dll

    def test_add_ascending_to_empty(self):
        for i, n in enumerate(self.nodes):
            with self.subTest():
                self.assertEqual(len(self.test_dll), i)
            self.test_dll + n

        with self.subTest():
            self.assertTrue(self.correct_dll == self.test_dll)

    def test_add_decending_to_empty(self):
        for i, n in enumerate(self.nodes[::-1]):
            with self.subTest():
                self.assertEqual(len(self.test_dll), i)
            self.test_dll + n

        with self.subTest():
            self.assertTrue(self.correct_dll == self.test_dll)

    def test_add_nodes_to_middle(self):
        for i, n in enumerate([0, 7, 4, 6, 5, 2, 3, 1]):
            with self.subTest():
                self.assertEqual(len(self.test_dll), i)
            self.test_dll + self.nodes[n]

        with self.subTest():
            self.assertTrue(self.correct_dll == self.test_dll)

    def test_update_values(self):
        for i, n in enumerate(self.nodes):
            with self.subTest():
                self.assertEqual(len(self.test_dll), i)
            self.test_dll + n

        self.copy_nodes[0].value = "123asdf"
        self.copy_nodes[7].value = "456;lkj"
        self.copy_nodes[2].value = "qwerty"
        self.test_dll + node("A", "123asdf")
        self.test_dll + node("H", "456;lkj")
        self.test_dll + node("C", "qwerty")

        with self.subTest():
            self.assertTrue(self.correct_dll == self.test_dll)

    def test_dll_bad_length(self):
        for n in self.nodes:
            self.test_dll + n

        self.test_dll - self.nodes[3].key
        self.test_dll - self.nodes[2].key
        self.test_dll + self.nodes[3]

        self.assertFalse(self.correct_dll == self.test_dll)

    def test_incorrect_node_value(self):
        for n in self.nodes:
            self.test_dll + n

        self.test_dll.sentinel.prev = self.test_dll.sentinel

        self.assertFalse(self.correct_dll == self.test_dll)

    def test_dll_get(self):
        for n in self.nodes:
            self.test_dll + n

        with self.subTest():
            self.assertEqual(self.test_dll.get(self.nodes[0].key), self.nodes[0].value)

        with self.subTest():
            self.assertEqual(self.test_dll.get(self.nodes[7].key), self.nodes[7].value)

        with self.subTest():
            self.assertEqual(self.test_dll.get(self.nodes[4].key), self.nodes[4].value)

        with self.subTest():
            self.assertIsNone(self.test_dll.get("12345abcde"))

    def test_dll_remove_sentinel(self):
        for n in self.nodes:
            self.test_dll + n

        self.correct_dll.sentinel = self.correct_dll.sentinel.next
        self.correct_dll.sentinel.prev = None
        self.correct_dll.length = 7
        self.test_dll - self.nodes[0].key

        self.assertTrue(self.correct_dll == self.test_dll)

    def test_dll_remove_last(self):
        for n in self.nodes:
            self.test_dll + n

        c = self.correct_dll.sentinel
        while c.next:
            c = c.next
        c = c.prev
        c.next = None

        self.correct_dll.length = 7

        self.test_dll - self.nodes[len(self.nodes) - 1].key

        self.assertTrue(self.correct_dll == self.test_dll)

    def test_dll_retrieve_noexistent_key(self):
        self.assertIsNone(self.correct_dll.get('12345abcdef'))

    def test_dll_remove_noexistent_key(self):
        self.assertIsNone(self.correct_dll - '12345abcdef')

    def test_dll_prints(self):
        x = \
        "key: A value: AA Prev:   Next: B\n" +\
        "key: B value: BB Prev: A Next: C\n" +\
        "key: C value: CC Prev: B Next: D\n" +\
        "key: D value: DD Prev: C Next: E\n" +\
        "key: E value: EE Prev: D Next: F\n" +\
        "key: F value: FF Prev: E Next: G\n" +\
        "key: G value: GG Prev: F Next: H\n" +\
        "key: H value: HH Prev: G Next:  \n"
        self.assertEqual(x, str(self.correct_dll))

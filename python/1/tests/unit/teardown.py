'''A base class that cleans up elements created in the set up base class'''

from .setup import SetUp

class TearDown(SetUp):
    '''Test the inserting and removal of nodes'''
    def tearDown(self):
        del self.a_node
        del self.b_node
        del self.c_node
        del self.d_node
        del self.e_node
        del self.f_node
        del self.g_node
        del self.h_node
        del self.nodes
        pass
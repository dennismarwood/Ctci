'''A base class that sets up common elements for tests'''

import unittest
from unittest.mock import patch, MagicMock, Mock
from copy import deepcopy
from ...node import node
import string

class SetUp(unittest.TestCase):
    '''Test the inserting and removal of nodes'''
    #pylint: disable=R0902,W0221
    def setUp(self):
        self.a_node = MagicMock(spec_set=node())
        self.b_node = MagicMock(spec_set=node())
        self.c_node = MagicMock(spec_set=node())
        self.d_node = MagicMock(spec_set=node())
        self.e_node = MagicMock(spec_set=node())
        self.f_node = MagicMock(spec_set=node())
        self.g_node = MagicMock(spec_set=node())
        self.h_node = MagicMock(spec_set=node())
        self.nodes = (self.a_node, self.b_node, self.c_node, self.d_node,
                      self.e_node, self.f_node, self.g_node, self.h_node)

        for i, letter in enumerate(string.ascii_uppercase[:8]):
            attrs = {'key': letter, 'value': letter*2, 'prev': None, 'next': None}
            self.nodes[i].configure_mock(**attrs)
        for i in range(0, len(self.nodes) - 1):
            self.nodes[i].configure_mock(next=self.nodes[i+1])
        for i in range(len(self.nodes) -1, 0, -1):
            self.nodes[i].configure_mock(prev=self.nodes[i-1])

        for n in self.nodes:
            r = "key: {0} value: {1} Prev: {2} Next: {3}".format(
                n.key,
                n.value,
                n.prev.key if n.prev else " ",
                n.next.key if n.next else " ")
            n.__repr__ = Mock(return_value=r)

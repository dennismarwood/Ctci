'''Tests the dlls module'''
from .teardown import TearDown
from ...node import node
from ...doublyLinkedList import doublyLinkedList
from ...dlls import dlls


class DllTestCase(TearDown):
    #pylint: disable=C0111
    #pylint: disable=W0104
    #pylint: disable=W0106
    #pylint: disable=W0201

    def setUp(self):
        super().setUp()
        self.correct_dll = doublyLinkedList(sentinel=self.copy_nodes[0])
        self.correct_dll.length = 8
        self.test_dll = doublyLinkedList()
        self.dll_list = dlls()

    def tearDown(self):
        super().tearDown()
        del self.correct_dll
        del self.test_dll
        del self.dll_list

    def test_dll_list_set_item(self):
        self.dll_list['bcvlz'] = 'zlvcb'
        x = "Stall:98\nkey: bcvlz value: zlvcb Prev:   Next: "

        with self.subTest():
            self.assertTrue(x in str(self.dll_list.print_dlls()))

        self.dll_list['aeohp'] = 'phoea'
        self.dll_list['aipyj'] = 'jypia'
        x = "Stall:99\nkey: aeohp value: phoea Prev:   "\
        "Next: aipyj\nkey: aipyj value: jypia Prev: aeohp Next: "
        with self.subTest():
            self.assertTrue(x in str(self.dll_list.print_dlls()))

    def test_dll_list_add_item(self):
        self.dll_list + node('bcvlz', 'zlvcb')
        x = "Stall:98\nkey: bcvlz value: zlvcb Prev:   Next: "
        with self.subTest():
            self.assertTrue(x in str(self.dll_list.print_dlls()))

        self.dll_list + node('aeohp', 'phoea')
        self.dll_list + node('aipyj', 'jypia')
        x = "Stall:99\nkey: aeohp value: phoea Prev:   "\
        "Next: aipyj\nkey: aipyj value: jypia Prev: aeohp Next: "
        with self.subTest():
            self.assertTrue(x in str(self.dll_list.print_dlls()))

    def test_dll_get_item(self):
        self.dll_list['bcvlz'] = 'zlvcb'
        self.dll_list['aeohp'] = 'phoea'
        self.dll_list['aipyj'] = 'jypia'

        self.assertEqual('phoea', self.dll_list['aeohp'])

    def test_dll_list_sub_item_middle(self):
        a = node('aeohp', 'phoea')
        b = node('aipyj', 'jypia')
        a.next = b
        b.prev = a
        c = node('ajxqg', 'gqxja')
        b.next = c
        c.prev = b
        self.dll_list.stalls[99].length = 3
        self.dll_list.stalls[99].sentinel = a
        self.dll_list - "aipyj"
        x = "Stall:99\nkey: aeohp value: phoea Prev:   "\
        "Next: ajxqg\nkey: ajxqg value: gqxja Prev: aeohp Next: "
        with self.subTest():
            self.assertTrue(x in str(self.dll_list.print_dlls()))
        with self.subTest():
            self.assertEqual(2, len(self.dll_list.stalls[99]))

    def test_dll_list_sub_item_first(self):
        a = node('aeohp', 'phoea')
        b = node('aipyj', 'jypia')
        a.next = b
        b.prev = a
        c = node('ajxqg', 'gqxja')
        b.next = c
        c.prev = b
        self.dll_list.stalls[99].length = 3
        self.dll_list.stalls[99].sentinel = a
        self.dll_list - "aeohp"
        x = "Stall:99\nkey: aipyj value: jypia Prev:   "\
        "Next: ajxqg\nkey: ajxqg value: gqxja Prev: aipyj Next: "
        with self.subTest():
            self.assertTrue(x in str(self.dll_list.print_dlls()))
        with self.subTest():
            self.assertEqual(2, len(self.dll_list.stalls[99]))

    def test_dll_list_sub_item_last(self):
        a = node('aeohp', 'phoea')
        b = node('aipyj', 'jypia')
        a.next = b
        b.prev = a
        c = node('ajxqg', 'gqxja')
        b.next = c
        c.prev = b
        self.dll_list.stalls[99].length = 3
        self.dll_list.stalls[99].sentinel = a
        self.dll_list - "ajxqg"
        x = "Stall:99\nkey: aeohp value: phoea Prev:   "\
        "Next: aipyj\nkey: aipyj value: jypia Prev: aeohp Next: "
        with self.subTest():
            self.assertTrue(x in str(self.dll_list.print_dlls()))
        with self.subTest():
            self.assertEqual(2, len(self.dll_list.stalls[99]))

    def test_add_delete_many_entries(self):
        #ENTRIES = set([''.join(random.sample(string.ascii_lowercase, 5)) for _ in range(10000)])
        #DEL_ENTRIES = [random.choice(list(ENTRIES)) for _ in range(10)]

        #for word in ENTRIES:
            #self.dll_list[word] = word[-1::-1] #Load up some dummy data

        self.assertEqual(True, True)
        #print(print_dlls())

        #for word in DEL_ENTRIES:
            #my_dict - word #Remove some dict entries pylint: disable=W0104

        #print_dlls()

        #for _ in range(5):
            #r = random.choice(list(ENTRIES))
            #print("What is the value for the key {0}? It is {1}.".format(r, my_dict[r]))
            #r = random.choice(list(ENTRIES))
            #print("What is the value for the key {0}? It is {1}.".format(r, my_dict[r]))

'''A base class that sets up common elements for tests'''

import unittest
from unittest.mock import patch, create_autospec
from copy import deepcopy
from ...node import node

class SetUp(unittest.TestCase):
    '''Test the inserting and removal of nodes'''
    #pylint: disable=R0902

    @patch('node.node')
    def setUp(self, MockClass):
        print("\nmock class")
        print(create_autospec(node))
        x = node("Z", "ZZZ")
        print("\nprint mock_class.key")
        print(x.key)
        #print(x.return_value)

        self.a_node = node("A", "AA")
        self.b_node = node("B", "BB")
        self.c_node = node("C", "CC")
        self.d_node = node("D", "DD")
        self.e_node = node("E", "EE")
        self.f_node = node("F", "FF")
        self.g_node = node("G", "GG")
        self.h_node = node("H", "HH")
        self.nodes = (self.a_node, self.b_node, self.c_node, self.d_node,
                      self.e_node, self.f_node, self.g_node, self.h_node)

        self.copy_nodes = deepcopy(self.nodes)
        leng = len(self.copy_nodes)

        for i in range(0, leng - 1):
            self.copy_nodes[i].next = self.copy_nodes[i+1]

        for i in range(leng -1, 0, -1):
            self.copy_nodes[i].prev = self.copy_nodes[i-1]

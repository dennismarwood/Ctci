'''
Acts like a python dict.
Overloads dunders to mimic dict.
'''

import hashlib
from .node import node
from .doublyLinkedList import doublyLinkedList

class dlls(): #pylint: disable=R0903
    '''dlls'''
    def __init__(self):
        self.stalls = [doublyLinkedList() for i in range(100)]

    def __getitem__(self, key):
        i = hashlib.sha224(key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        return self.stalls[i].get(key)

    def __setitem__(self, key, value):
        i = hashlib.sha224(key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        self.stalls[i] + node(key, value) #pylint: disable=W0106

    def __add__(self, new_node):
        '''You can use + if you want, but you have to pass a node.'''
        i = hashlib.sha224(new_node.key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        self.stalls[i] + new_node #pylint: disable=W0104

    def __sub__(self, target_key):
        i = hashlib.sha224(target_key.encode('utf-8')).hexdigest()
        i = int(str(int(i, 16))[-2:])
        self.stalls[i] - target_key #pylint: disable=W0104

    def print_dlls(self):
        '''print all the dll'''
        res = "\n"
        for i, d in enumerate(self.stalls):
            if str(d) != "":
                res += "Stall:{0}\n{1}".format(str(i), str(d))
        res += "How many Nodes per dll? (Collision vs Memory)\n"
        for i, d in enumerate(self.stalls):
            res += "{0} - {1}\n".format(str(i), str(len(d)))
        return res

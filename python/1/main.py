'''
A Hash Table data structure. https://en.wikipedia.org/wiki/Hash_table
Simulates python's dict functionality.
'''

import random
import string

ENTRIES = set([''.join(random.sample(string.ascii_lowercase, 5)) for _ in range(10000)])
DEL_ENTRIES = [random.choice(list(ENTRIES)) for _ in range(10)]

if __name__ == "__main__":
    # execute only if run as a script
    pass

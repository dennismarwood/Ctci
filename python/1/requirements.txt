astroid==1.5.2
coverage==4.4.1
isort==4.2.5
lazy-object-proxy==1.3.1
mccabe==0.6.1
pkg-resources==0.0.0
pylint==1.7.1
six==1.10.0
wrapt==1.10.10

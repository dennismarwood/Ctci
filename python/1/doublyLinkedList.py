'''
Handles collisions that occur during a hash.
Manages nodes.
'''

import hashlib
from .node import node

class doublyLinkedList(): #pylint: disable=R0903
    '''Holds Nodes. Overloads + and - and has get method.'''

    def __init__(self, sentinel=None):
        self.sentinel = sentinel
        self.length = 0

    def __eq__(self, other_dll):
        assert isinstance(other_dll, doublyLinkedList)

        if len(self) != len(other_dll):
            raise ValueError('{0} <> {1}'.format(str(len(self)), str(len(other_dll))))
        s_cursor = self.sentinel
        o_cursor = other_dll.sentinel
        for _ in range(len(self)):
            if str(s_cursor) != str(o_cursor):
                return False
            s_cursor = s_cursor.next
            o_cursor = o_cursor.next
        return True

    def get(self, key): #pylint: disable=C0111
        cursor = self.sentinel
        print(self.sentinel)
        while cursor:
            if cursor.key == key:
                return cursor.value
            cursor = cursor.next
        return None

    def __len__(self):
        return self.length

    def __add__(self, new_node):
        assert isinstance(new_node, node)
        self.length += 1

        #list is empty
        if not self.sentinel:
            self.sentinel = new_node
            return

        cursor = self.sentinel
        while cursor:
            #User updating key's value
            if cursor.key == new_node.key:
                cursor.value = new_node.value
                self.length -= 1
                return

            #key is smaller than any in the list
            if cursor.key > new_node.key:
                new_node.next = cursor
                cursor.prev = new_node
                self.sentinel = new_node
                return

            if cursor.next:

                #Move forward if the next key is smaller than the new
                if cursor.next.key <= new_node.key:
                    cursor = cursor.next
                    continue

                #Adding to middle of list
                cursor.next.prev = new_node
                new_node.prev = cursor
                new_node.next = cursor.next
                cursor.next = new_node
                return

            #Key is bigger than any in the list
            cursor.next = new_node
            new_node.prev = cursor
            return

    def __sub__(self, target_key):
        assert isinstance(target_key, str)
        #identify node stall
        self.length -= 1
        stall = hashlib.sha224(target_key.encode('utf-8')).hexdigest()
        stall = int(str(int(stall, 16))[-2:])

        cursor = self.sentinel

        while cursor:
            if cursor.key < target_key:
                cursor = cursor.next
                continue

            if cursor.key == target_key:
                if cursor.next:
                    cursor.next.prev = cursor.prev
                if cursor.prev:
                    cursor.prev.next = cursor.next
                #May need to update the sentinel
                if cursor == self.sentinel:
                    self.sentinel = cursor.next
                cursor = None
                return True

            cursor = cursor.next
        #Key was not present
        self.length += 1
        return None


    def __repr__(self):
        result = ""
        cursor = self.sentinel
        while cursor:
            result += str(cursor) + "\n"
            cursor = cursor.next
        return result

'''A node will hold the data the user wants to store'''

class node(object):
    #pylint: disable=R0903
    '''Node in a doublelinked list. User must give a key value.'''

    def __init__(self, key=None, value=None):
        self.key = key
        self.value = value
        self.prev = None
        self.next = None

    def __repr__(self):
        return "key: {0} value: {1} Prev: {2} Next: {3}".format(self.key,
                                                                self.value,
                                                                self.prev.key if self.prev else " ",
                                                                self.next.key if self.next else " ")

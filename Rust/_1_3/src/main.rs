/*
Write a method to replace all the spaces in a string with "%20". You may assume that the string has sufficient space at the end to hold the additional characters, and that you are given the "true" length of the string.
*/

fn main() {
    let s = String::from("Mr John Smith    ").trim().replace(" ", "%20");
    println!("-{}-", s);
}

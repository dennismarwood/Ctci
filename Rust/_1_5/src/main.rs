/*
There are three types of edits that can be performed on strings: insert a character, remove a character, or replace a character. Given two strings, write a function to check if they are one edit (or zero edits) away.
*/

/*
Place smaller word into w1.
If w1.len < w2.len then allow for insertion on first mismatch.
Else, allow for one substitution.
*/

use std::mem;
use std::fmt;

fn main() {
    let mut w1 = Word::new("pale".to_string());
    let mut w2 = Word::new("ple".to_string());

    if w1.length > w2.length {
        mem::swap(&mut w1, &mut w2);
    }

    
    println!("Words: {} {}", w1, w2);

    for (i, c) in w2.word.iter().enumerate() {
        match w1.word.get(i) {
            Some(char) =>{
                if *c != w1.word[i] {
                    if w1.length < w2.length {
                        println!("Inserting '{}' before '{}' in {}", *c, w1.word[i], w1);
                        w1.word.insert(i, w2.word[i]);
                    } else {
                        println!("Substituting '{}' for '{}' in {}", *c, w1.word[i], w1);
                        w1.word[i] = w2.word[i];
                    }
                    break;
                }
            },
            None =>{ //Words were only different by a letter on the end
                println!("Appending {} to the end of {}", *c, w1);
                w1.word.push(*c);
                break;
            }
        }
    }

    if w1.word == w2.word {
        println!("Words are equal with 1 or less changes");
    } else {
        println!("Words require 2 or more changes to be equal");
    }
    println!("Words: {} {}", w1, w2);
}

struct Word {
    length: usize,
    word: Vec<char>,
}

impl Word {
    fn new(s: String) -> Word {
        Word{
            word: s.chars().collect(),
            length: s.len(),
        }
    }
}

impl fmt::Display for Word {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s: String = self.word.iter().collect();
        write!(f, "{}", s)
    }
}

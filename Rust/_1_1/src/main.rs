/*
Is Unique: Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?
*/

use std::collections::HashMap;

fn main() {
    let text = "abcdef g hiaj asdf ee kvniengo/z   dd";
    //let text = "abcdef";

    let mut chars = HashMap::new();
    
    //https://doc.rust-lang.org/book/ch08-03-hash-maps.html#only-inserting-a-value-if-the-key-has-no-value
    for word in text.split_whitespace(){
        for c in word.chars() {
            let count = chars.entry(c).or_insert(0); //If cannot find c, add key c and value 0
            *count += 1;
        }
    }

    println!("Is unique {:?}", is_unique(&chars));
    println!("Is unique 2 {:?}", is_unique_2(&text));
    println!("{:?}", chars);
}

fn is_unique(chars: &HashMap<char, i8>) -> bool {
    for (k, v) in chars.into_iter()
                        .filter(|(_k, v) | (**v > 1)) {
        println!("Letter '{}' appeared {} times.", k, v);
        return false
    }
    true
}

fn is_unique_2(string: &str) -> bool {
    let mut chars = String::new();
    //In rust, strings are a data structure
    for word in string.split_whitespace(){
        for c in word.chars() {
            if chars.contains(c) {
                return false
            }
            else {
                chars.push(c);
            }
        }
    }
    true
}

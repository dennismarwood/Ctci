/*
Given two strings, write a method to decide if one is a permutation of the other.
*/

use std::ops::Div;

fn main() {
    let string_1 = AString::new(String::from("dog"));
    let string_2 = AString::new(String::from("gOd"));

    print!("{} is a permutation of {}: ", string_1.s, string_2.s);
    println!("{}", string_1 / string_2);

    let string_1 = AString::new(String::from("hOG"));
    let string_2 = AString::new(String::from("Godh"));

    print!("{} is a permutation of {}: ", string_1.s, string_2.s);
    println!("{}", string_1 / string_2);
}

struct AString {
    s: String,
}

impl AString {
    fn new(s: String) -> AString {
        AString {
            s: s,
        }
    }
}

impl Div for AString { //Just to mess with operator overloading
    type Output = bool;

    fn div(self, rhs: Self) -> Self::Output {
        let mut l: Vec<char> = self.s.to_lowercase().chars().collect(); //.chars is lazy
        l.sort();

        let mut r: Vec<char> = rhs.s.to_lowercase().chars().collect();
        r.sort();

        if l == r {
            return true;
        }
        false
    }
}